import java.lang.reflect.Constructor;
import java.lang.reflect.InvocationTargetException;
import java.util.Arrays;

//Use reflection methods to extract private fields/methods, instantiate objects and use constructors
public class Controller {
    public static void main(String[] args) throws IllegalAccessException, InstantiationException, NoSuchMethodException, InvocationTargetException {
        Car car = new Car(300,3,"Tesla");
        Class<? extends Car> aClass = car.getClass();
        Arrays.stream(aClass.getDeclaredFields()).forEach(field -> System.out.println(field));
        Arrays.stream(aClass.getDeclaredMethods()).forEach(field -> System.out.println(field));
        Arrays.stream(aClass.getDeclaredConstructors()).forEach(field -> System.out.println(field));

        Constructor<? extends Car> constructor = aClass.getConstructor(int.class, int.class, String.class);
        Car car1 = constructor.newInstance(2,4,"BMW");

    }

}

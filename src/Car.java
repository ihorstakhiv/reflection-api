public class Car {
    private int hP;
    private int engineV;
    private String model;

    public Car(int hP, int engineV, String model) {
        this.hP = hP;
        this.engineV = engineV;
        this.model = model;
    }

    private void swapEngine(int engineV){
        this.engineV = engineV;
    }

    public int gethP() {
        return hP;
    }

    public void sethP(int hP) {
        this.hP = hP;
    }

    public int getEngineV() {
        return engineV;
    }

    @Override
    public String toString() {
        return "Car{" +
                "hP=" + hP +
                ", engineV=" + engineV +
                ", model='" + model + '\'' +
                '}';
    }

    public void setEngineV(int engineV) {
        this.engineV = engineV;
    }

    public String getModel() {
        return model;
    }

    public void setModel(String model) {
        this.model = model;
    }
}
